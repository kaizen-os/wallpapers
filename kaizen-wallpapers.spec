%global srcname wallpapers
%global default_wallpaper "Mount Fuji by Clay Banks.jpg"

Name:           kaizen-wallpapers
Version:        0.9
Release:        1%{?dist}
Summary:        The default wallpapers for Kaizen
License:        GPLv3+
URL:            https://gitlab.com/kaizen-os/%{srcname}
Source0:        %{url}/-/archive/%{version}/%{srcname}-%{version}.tar.gz

BuildRequires:  coreutils

BuildArch: noarch

%description
The default wallpapers for Kaizen

%prep
%autosetup -n %{srcname}-%{version}

%install
# copy wallpapers to install location
mkdir -p %{buildroot}/%{_datadir}/backgrounds/kaizen
cp -pav *.jpg %{buildroot}/%{_datadir}/backgrounds/kaizen

# create default wallpaper symlink
# TODO: update kaizen-default symlink path here, in default-settings, and sb plug
ln -s ./%{default_wallpaper} %{buildroot}/%{_datadir}/backgrounds/kaizen/kaizen-default

%files
%{_datadir}/backgrounds/

%changelog
* Tue Oct 23 2018 Cody Garver <codygarver@gmail.com> - 0.9-1
- Initial version of the package
